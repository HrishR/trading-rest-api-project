package com.example.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Stock;
import com.example.demo.repository.StockRepository;

@ActiveProfiles("h2")
@SpringBootTest
public class StockServiceTest {

	
	@Autowired
	StockService stockService;


	@MockBean
	StockRepository stockRepository;
	
	
	
	@Test
	public void testGetStockById() {
		
		int testId = 5;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		
		when(stockRepository.getStockById(testStock.getId()))
		.thenReturn(testStock);
		
		
		assertThat(stockService.getStockById(testId)).isEqualTo(testStock);
	}
	
	@Test
	public void testDeleteStock() {
		int testId = 5;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		when(stockRepository.deleteStock(testId)).thenReturn(testId);
		
		assertThat(stockService.deleteStock(testId)).isEqualTo(testId);
	}
	
	@Test
	public void testAddStock() {
		int testId = 5;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		when(stockRepository.addStock(testStock)).thenReturn(testStock);
		
		assertThat(stockService.addStock(testStock)).isEqualTo(testStock);
	}
	
	@Test
	public void testEditStock() {
		int testId = 5;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		when(stockRepository.editStock(testStock)).thenReturn(testStock);
		
		assertThat(stockService.editStock(testStock)).isEqualTo(testStock);
	}
	
	@Test
	public void testGetAllStocks() {
		List<Stock> returnedList = new ArrayList<Stock>();
		
		when(stockRepository.getAllStocks()).thenReturn(returnedList);
		
		assertThat(stockService.getAllStocks()).isEqualTo(returnedList);
	}
	
	@Test
	public void testGetStockByTicker() {
		int testId = 5;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		List<Stock> returnedList = new ArrayList<Stock>();
		
		when(stockRepository.getStockByTicker(testTckr)).thenReturn(returnedList);
		
		assertThat(stockService.getStockByTicker(testTckr)).isEqualTo(returnedList);
	}
	
}
