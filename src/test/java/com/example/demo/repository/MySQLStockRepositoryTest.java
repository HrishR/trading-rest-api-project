package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Stock;

@ActiveProfiles("h2")
@SpringBootTest
public class MySQLStockRepositoryTest {

	@Autowired
	MySQLStockRepository mySQLStockRepository;
	
	@Test
	public void testGetAllStocks() {
		List<Stock> returnedList = mySQLStockRepository.getAllStocks();
		
		assertThat(returnedList).isNotEmpty();
	}
	
	@Test
	public void testGetStockByTicker() {
		List<Stock> returnedList = mySQLStockRepository.getStockByTicker("AAPL");
		
		assertThat(returnedList).isNotEmpty();
	}
	
	@Test
	public void testGetStockById() {
		assertThat(mySQLStockRepository.getStockById(1).getStockTicker()).isEqualTo("AAPL");
	}
	
	@Test
	public void testEditStock() {
		int testId = 5;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		assertThat(mySQLStockRepository.editStock(testStock)).isEqualTo(testStock);
	}
	
	@Test
	public void testAddStock() {
		int testId = 6;
		String testTckr = "testStock";
		double testPrice = 1.00;
		int testVolume = 100;
		String testBuyOrSell = "Buy";
		int testStatusCode = 0;
		LocalDateTime testDateTime = LocalDateTime.now();
		
		Stock testStock = new Stock();
		
		testStock.setId(testId);
		testStock.setStockTicker(testTckr);
		testStock.setBuyOrSell(testBuyOrSell);
		testStock.setDateTime(testDateTime);
		testStock.setPrice(testPrice);
		testStock.setStatusCode(testStatusCode);
		testStock.setVolume(testVolume);
		
		assertThat(mySQLStockRepository.addStock(testStock)).isEqualTo(testStock);
	}
	
	@Test
	public void testDeleteStock() {
		int testId = 5;
		
		assertThat(mySQLStockRepository.deleteStock(testId)).isEqualTo(testId);
	}
}
