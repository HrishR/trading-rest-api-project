package com.example.demo.repository;



import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.example.demo.entities.Stock;

@Repository
public class MySQLStockRepository implements StockRepository{

	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<Stock> getAllStocks() {
		String query="SELECT * FROM Stock ORDER BY dtime DESC";
		return template.query(query, new StockRowMapper());
	}

	@Override
	public Stock getStockById(int id) {
		String query="SELECT * FROM Stock WHERE id=?";
		return template.queryForObject(query, new StockRowMapper(),id);
	}

	@Override
	public Stock editStock(Stock stock) {
		String query="UPDATE stock SET stockTicker=?, price=?, volume=?, buyOrSell=?, statusCode=? WHERE id=?";
		template.update(query,stock.getStockTicker(),stock.getPrice(),stock.getVolume(),stock.getBuyOrSell(),stock.getStatusCode(),stock.getId());
		return stock;
	}

	@Override
	public int deleteStock(int id) {
		String query="DELETE FROM Stock WHERE id=?";
		template.update(query,id);
		return id;
	}

	@Override
	public Stock addStock(Stock stock) {
		String query="INSERT INTO Stock(stockTicker, price, volume, buyOrSell, statusCode) " +"VALUES(?,?,?,?,?)";
		template.update(query,stock.getStockTicker(),stock.getPrice(),stock.getVolume(),stock.getBuyOrSell(),stock.getStatusCode());
		return stock;
	}

	@Override
	public List<Stock> getStockByTicker(String stockTicker) {
		String query="SELECT * FROM Stock WHERE stockTicker=?";
		return template.query(query, new StockRowMapper(),stockTicker);
	}

}

class StockRowMapper implements RowMapper<Stock>{

	@Override
	public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Stock(rs.getInt("id"),rs.getString("stockTicker"),rs.getDouble("price"),
				rs.getInt("volume"),rs.getString("buyOrSell"),rs.getInt("statusCode"), rs.getTimestamp("dtime").toLocalDateTime());
	}
	

}
