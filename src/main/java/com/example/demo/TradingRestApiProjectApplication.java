package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradingRestApiProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradingRestApiProjectApplication.class, args);
	}

}
