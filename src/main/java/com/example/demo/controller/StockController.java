package com.example.demo.controller;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entities.Stock;
import com.example.demo.service.StockService;

@CrossOrigin("*")
@RestController
@RequestMapping("api/stocks")
public class StockController {
	
	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);
	
	@Autowired
	private StockService service;
	
	@GetMapping
	public List<Stock> getAllStocks() {
		return service.getAllStocks();		
	}
	
	@GetMapping(value = "/{id}")
	public Stock getStockById(@PathVariable("id") int id) {
		LOG.debug("getStockById, id=[" + id + "]");
	  return service.getStockById(id);
	}

	@PostMapping
	public Stock addStock(@RequestBody Stock stock) {
		return service.addStock(stock);
	}

	@PutMapping
	public Stock editStock(@RequestBody Stock stock) {
		return service.editStock(stock);
	}

	@DeleteMapping(value = "/{id}")
	public int deleteStock(@PathVariable int id) {
		return service.deleteStock(id);
	}
	
	@GetMapping(value = "/tckr/{tckr}")
	public List<Stock> getStockByTicker(@PathVariable String tckr) {
		return service.getStockByTicker(tckr);		
	}


}
