package com.example.demo.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Stock;
import com.example.demo.repository.StockRepository;

@Service
public class StockService {
	@Autowired
	private StockRepository repo;
	
	public List<Stock> getAllStocks(){
		return repo.getAllStocks();
	}
	
	public Stock getStockById(int id) {
		return repo.getStockById(id);
	}
	
	public Stock editStock(Stock stock) {
		return repo.editStock(stock);
	}
	
	public int deleteStock(int id) {
		return repo.deleteStock(id);
	}
	
	public Stock addStock(Stock stock) {
		return repo.addStock(stock);
	}
	
	public List<Stock> getStockByTicker(String stockTicker){
		return repo.getStockByTicker(stockTicker);
	}

}
